# Istraživanje YOLOP i sličnih algoritama

- Svo istraživanje objašnjeno je unutar ove datoteke i unutar merge requesta.
- Korišteni algoritmi:
  - [Real time lane detection by Addison Sears-Collins](https://automaticaddison.com/the-ultimate-guide-to-real-time-lane-detection-using-opencv/)
  - [Curved Lane Lines](https://github.com/kemfic/Curved-Lane-Lines)
  - [YOLOP](https://github.com/hustvl/YOLOP)

#

# Segmentacija cesta i rubnjaka

## Uvod

- Segmentacija cesta i rubnjaka popularan je problem kojim se bave mnogi programeri prilikom razvijanja autonomnih vozila.
- Uz mnoge druge senzore, kamera predstavlja vrlo bitan faktor u autonomnoj vožnji. Ona se kao takva koristi za dobivanje raznih podataka o okolini.
- Računala i način na koji je njihova snaga bila iskorištavana, bila su vrlo neefikasna u zadacima semantičke segmentacije.
  - Klasično programiranje jednostavno se nije moglo koristiti jer je potrebno kreirati beskonačno mnogo uvjetovanja i grananja programskog koda.
  - Kao rješenje tome problemu, pojavili su se razni oblici strojnog učenja i prethodnici umjetne inteligencije.
  - Neuronske mreže primjer su programiranja algoritama koji mogu povezivati odnose između podataka (npr. asocijacija) po uzoru na način rada ljudskog mozga. Njihova primjena uvelike je ubrzala razvoj umjetne inteligencije i dovela algoritme na novu razinu.

#

# YOLOP

## Jače strane YOLOP algoritma

- YOLOP algoritam pokazao se kao iznimno dobar prilikom segmentacije cesta na izvangradskim prometnicama.
- Na gradskim prometnicama većinu je vremena bio precizan u segmentaciji cesta i rubnjaka
- Iako izvan cilja projektnog zadatka, program se pokazao kao vrlo pouzdan alat za detekciju ostalih vozila u prometu.
  - Čak i kad se programu dao video lošije kvalitete.
  - Doduše, program je imao nekoliko lažno pozitivnih detekcija, no, u ovom je slučaju manji rizik za lažno pozitivne nego za lažno negativne detekcije.

![](pictures/yolop_01.png)
![](pictures/yolop_02.png)
![](pictures/yolop_03.png)
![](pictures/yolop_04.png)
![](pictures/yolop_05.png)

## Problemi prilikom segmentacije cesta i rubnjaka

- Neki od čestih problema su vozila koja dolaze iz suprotnog smjera.

  - Noću vozila imaju uključena svjetla i često nakratko “oslijepe” kameru kojom se vrši segmentacija.
  - U sustavu koji mora funkcionirati gotovo savršeno, ovakav problem može podosta narušiti sigurnost u prometu.
  - Navedeni se problem znatno pojačava ako iz suprotnog smjera dolazi više vozila s uključenim svjetlima.
    ![](pictures/yolop_mana_01.png)

- Postoji i problem s rubnjacima kada je rubnjak vrlo nizak i blago drugačije nijanse od ceste.
  - Program tada lažno pozitivno detektira cestu i preko rubnjaka i pločnika što je svakako nedopustivo i potrebno je dodatno trenirati model.
  - Ovo predstavlja dosta veliki problem ako se detektiranje rubnjaka ne dovede na pristojnu razinu ili se ne implementiraju bolji algoritmi kao sigurnosni slojevi.
    ![](pictures/yolop_mana_02.png)

## Testiranje algoritama za segmentaciju cesta

- Vlastite algoritme možemo testirati na skupinama podataka koje se u zajednici koriste kao referentna točka za preciznost rada algoritma.
- Vrlo popularan primjer je [KITTI](http://www.cvlibs.net/datasets/kitti/) skupina testnih podataka s njihovim rješenjima.
  - Što je naš algoritam bliži rješenjima, to je bolje rangiran (u postocima).
- Drugi vrlo popularan set podataka je [Berkeley DeepDrive](https://bdd-data.berkeley.edu/) set podataka koji je također prihvaćen od strane zajednice kao mjerilo preciznosti algoritama za segmentaciju cesta i mnogih drugih informacija u prometu.

## Zaključak

- Korištenjem YOLOP skupa alata za segmentaciju cesta i rubnjaka postiže se prilično dobra preciznost prilikom segmentacije.
- Za najbolju brzinu izvršavanja, program zahtjeva Nvidia grafičku karticu sa što većim brojem CUDA jezgri.
  - No, iako pokrenut na uređaju s grafičkom karticom koja koristi CUDA jezgre, program je ovisan o mnogim Python bibliotekama koje moraju što bolje implementirati same CUDA jezgre.
  - U mom primjeru, pokretanje na CUDA jezgrama nije bilo moguće zbog iznimaka unutar biblioteka koje koristi program.
- YOLOP mnogo sporije obrađuje video pomoću procesorske snage te je tako za 1080p video na 60 sličica po sekundi i duljine 5 minuta, program trebao više od sat vremena za obradu videozapisa.
  - Treba napomenuti da je ovo vrijeme višestruko bolje u CUDA načinu rada te na grafičkim karticama koje su specijalizirane za to.
- Nvidia je daleko ispred popularne konkurencije što se tiče grafičke obrade.
  - Većina novijih Nvidia grafičkih kartica mogu pokretati programe poput YOLOP-a za razne obrade videozapisa, bilo real-time ili na unaprijed snimljenim videozapisima.
  - Njihova superiornost dodatno se ističe i alatima koje Nvidia nudi razvojnim programerima.

# Ostali algoritmi

## Real time lane detection by Addison Sears-Collins

### Zapažanja

- Iako jednostavniji, ovaj se algoritam izrazito dobro ponašao prilikom segmentacije izvangradskih cesta poput autoputova
  ![](pictures/automatic_addison_01.png)
  - Razlog tome je što su autoputovi puno pregledniji i imaju mnogo blaže promjene smjera kretanja za razliku od cesta unutar grada
- Velika mana ovog algoritma izražena je u gradskom prometu
  ![](pictures/automatic_addison_02.png)
  - Uz to, algoritam ovisi o definiranju interesnih točaka te samim time nije automatski prilagodljiv situaciji već je potrebno ručno podešavanje ovisno o poziciji kamere i slično.

#

## Curved Lane Lines

### Zapažanja

- Kao i prethodni algoritam, i ovaj se pokazao kao odličan na izvangradskim prometnicama, no unutar grada daje konstantno loše rezultate
  ![](pictures/p4_01.png)
  ![](pictures/p4_02.png)

#
