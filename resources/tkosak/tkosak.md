<h1>Traffic sign detection using OpenCV</h1>

<h2>RIRV projektni zadatak: Detekcija prometnih znakova</h2>

Cilj ovog projektnog zadatka bio je detekcija prometnih znakova. Ova vrsta tehnologija aktivno se razvija iz dana u dan
s velikom količinim novaca koja se ulaže u ovu granu tehnologije. U ovome slučaju koristila se OpenCV tehnologija u
python okruženju, no to nije jedini način da se postigne detekcija prometnih znakova. U današnje vrijeme puno se koristi
AI i neuronske mreže no ovdje je odabrano jednostavnije rješenje.

Za izradu ovog projekta korišten je OpenCV python modul (Open source Computer Vision). To je library otvorenog koda koji
sadrži nekoliko stotina algoritama u području računalnog vida.
OpenCV koristi modularnu strukturu što znači da uključuje nekoliko librarya.

Neki osnovni moduli OpenCV-a su:

Core funcionality - koji definira osnovne strukture podataka i osnovne funkcije koje se koriste u drugim modulima
Image proccesing  imgproc - za obradu slike, linearno i nelinearno filtriranje, geometrijske tranformacije, histograme, pretvorbu boja...
Video analysis video - procjena pokreta, subtraction pozadine, algoritmi za praćenje objekata
Video I/O videoio - sučelje jednostavno za korištenje za snimanje videa.
2D Features Framework features2d - istaknuti detektori značajki, deskriptori i uparivači deskriptora.
Object Detection objdetect - otkrivanje objekata i instanci unaprijed definiranih klasa


<h3>IZRADA PROJEKTA</h3>

Projektni zadatak se bazirao na videima vožnje po gradu, koji su se analizirali i na njima su se izrađivali projektni zadaci.
Korišteni su algoritmi za detekciju (Cascade Classifier), moduli za 'čitanje' videa, prikaz i sl. (Video Capture, imshow, ...)

Prvi dio zadatka se odnosi na dohvaćanje i prikaz videa:



    cap = cv2.VideoCapture('DayDrive1.mp4')
Video capture module

Klasa VideoCapture služi u za snimanje/dohvaćanje videa iz video zapisa, datoteka, niza slika... Objekt kao argument prima ime datoteke koji zatim spremamo u varijablu cap (captutre).

    stop = cv2.CascadeClassifier('stop_data.xml')
    #circular = cv2.CascadeClassifier('circular_cascade.xml')
    #triangular = cv2.CascadeClassifier('triangular_cascade.xml')

U ove tri linije učitavaju se XML datoteke, kaskadne klasifikatore, tj. klasifikatore koji radi sa haar-značajkama, poseban je slučaj učenja koji se naziva boosting (hrv. pojačavanje),  definiranim unutar XML datoteka.
Haar-like features su digitalne slike koje se koriste u svrhe prepoznavanja objekata. 
Kaskadni klasifikatori se treniraju na nekoliko stotina slika slike koja sadrži objekt koji želimo detektirati, a i drugih slika koje ne sadrže te slike.





Sljedeći dio se odvija unutar while petlje:

    while True:
        ret, frame = cap.read()
    #čita se cap, odnosno video

        # Our operations on the frame come here
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        #openCV otvara slike u BRG ali za nase procesuiranje potreban je grayscale pa pretvaramo sliku(video) u grayscale


        signs = stop.detectMultiScale(gray, 1.9, 1)
        #signs = triangular.detectMultiScale(gray, 1.9, 1)
        #signs = circular.detectMultiScale(gray, 1.9, 1)

        #(zbog performansi pokretanja videa, klasifikatori su razdvojeni i pokretani zasebno)
        #na grayscale videu pokreču se algoritmi za detekciju prema zadanim klasifikatorima

        # Display the resulting frame
        for (x, y, w, h) in signs:
        cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
        #za detektirane oblike prema klasifikatoru iscrtava se okvir
        cv2.imshow('frame', frame) 
        #prikaz okvira

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    #za prekid prikazivanja videa čeka se pritisak na tipku q
    
    

 When everything done, release the capture

    cap.release()

    cv2.destroyAllWindows()
    #Ako se ne zaustavi ručno pri završetku videa automatski se gasi.

<h1>Zaključak</h1>

Ovaj način radi djelomično, za točnije rezultate bolje je koristiti neuronske mreže s dobro istreniranim modelima. U ovom slučaju gledala se jednostavnost izrade projekta.


Moguća je optimizacija samih klasifikatora za bolje rezultate.
Npr. Znak stop se detektira vrlo dobro ali ponekad se pojave artefakti koje klasifikator prepozna približno kao znak stop i označi ga. Međutim to je trash data.

Dobro detektiran znak stop

![stop_sign_detected.png](stop_sign_detected.png) 

Trash data. Detaktirano je nešto nepotrebno

![img.png](img.png)

Također moguća optimizacija izvođenja projekta,
na primjer moguće je odrezati 30% gornje i 30% doljne strane videa, pošto prometni znakovi se neće pojavljivati u tim područjima.

Korištenjem Kaskadnih klasifikatora projekt je napravljen na jedan vrlo jednostavan način. Postoje bolja rješenja, poput CNN odnosno konvolucijskih neuronskih mreža s istreniranim modelima koji bi bili precizniji u ovome projektu.