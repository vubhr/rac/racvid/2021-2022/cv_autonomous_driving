import cv2
from matplotlib import pyplot as plt

cap = cv2.VideoCapture('DayDrive1.mp4')

stop = cv2.CascadeClassifier('stop_data.xml')
circular = cv2.CascadeClassifier('circular_cascade.xml')
triangular = cv2.CascadeClassifier('triangular_cascade.xml')

while True:
# Opening image

# OpenCV opens images as BRG
# but we want it as RGB We'll
# also need a grayscale version

    ret, frame = cap.read()

    # Our operations on the frame come here
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    signs = stop.detectMultiScale(gray, 1.9, 1)
    #signs = triangular.detectMultiScale(gray, 1.9, 1)
    #signs = circular.detectMultiScale(gray, 1.9, 1),

    # Display the resulting frame
    for (x, y, w, h) in signs:
        cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)

    cv2.imshow('frame', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
